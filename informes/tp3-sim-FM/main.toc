\babel@toc {spanish}{}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Objetivo}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Implementaci\IeC {\'o}n}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}Desarrollo (en tiempo y frecuencia)}{1}{section.1.3}
\contentsline {section}{\numberline {1.4}Presentaci\IeC {\'o}n}{1}{section.1.4}
\contentsline {section}{\numberline {1.5}Puntos de medici\IeC {\'o}n (relacionados con el esquema de implementaci\IeC {\'o}n)}{2}{section.1.5}
\contentsline {chapter}{\numberline {2}Capturas}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Esquem\IeC {\'a}tico de simulaci\IeC {\'o}n}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}\IeC {\'I}ndice de modulaci\IeC {\'o}n = 5}{4}{section.2.2}
\contentsline {section}{\numberline {2.3}\IeC {\'I}ndice de modulaci\IeC {\'o}n = 25}{6}{section.2.3}
\contentsline {section}{\numberline {2.4}\IeC {\'I}ndice de modulaci\IeC {\'o}n = 30}{8}{section.2.4}
